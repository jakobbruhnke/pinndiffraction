# PINNDiffraction

Solving the Paraxial Helmholtz equation with physics-informed neural networks.

## Installation

If no GPU is used, `tensorflow` in a conda environment should do the trick. Maybe `Keras` needs to be installed separately (at least on Windows; on Debian-based systems everything works fine).

If GPU acceleration is wanted, it depends on the system, unfortunately. See the [tensorflow website](https://www.tensorflow.org/install/source_windows) on the required version compatibility between `tensorflow-gpu` and `CUDA`.

## Roadmap

- [x] create repository
- [x] brush up on some optic basics
- [ ] [invite](https://docs.gitlab.com/ee/user/project/members/) supervisors
- [ ] implement PINN for paraxial differential equation
    - [ ] first solution: paraxial waves
    - [ ] second solution: gaussian waves

## Authors and acknowledgment

**Author**: Jakob Bruhnke

**Supervisors:** Dr. Jisha Chandroth Pannian & Dr. Alessandro Alberucci

**Referees:** Prof. Dr. Stefan Nolte & Dr. Jisha Chandroth Pannian