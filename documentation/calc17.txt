Documentation for Calculation 17
2D Gaussian beam, normalized
Boundary condition: dirichlet
Optimizer: Adam till 5000, L-BFGS from 5000 to 7500
5 Layers and 32 nodes per layer
Learning rate is 0.001, residual weights is 0.001
Bound weight is 1 and initial weight is 1.0
A_0 = 1.0 and z_0 = 1.0
x_lb = -1.0 and x_ub = 1.0
z_lb = 0.0 and z_ub = 1.0
N = 10000 and N_test = 101 and N_b / N_0 = 200
Curriculum = TRUE, origin is calc16
EXTRA INFO: ---